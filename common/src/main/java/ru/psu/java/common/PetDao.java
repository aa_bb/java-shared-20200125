package ru.psu.java.common;

import java.util.stream.Stream;

public interface PetDao {
    void save(Pet pet);
    Stream<Pet> getAll();
}
