package ru.psu.java.common;

import java.time.LocalDate;

public class Pet {
    String name;
    LocalDate birthday;

    public Pet(String name, LocalDate birthday) {
        this.name = name;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String prettyString() {
        return String.format("%s (%s)",
                getName(),
                getBirthday().toString());
    }
}
