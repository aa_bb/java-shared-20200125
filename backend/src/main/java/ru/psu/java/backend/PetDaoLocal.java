package ru.psu.java.backend;

import ru.psu.java.common.Pet;
import ru.psu.java.common.PetDao;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class PetDaoLocal implements PetDao {
    private final List<Pet> pets = new ArrayList<Pet>();

    public void save(Pet pet) {
        pets.add(pet);
    }

    public Stream<Pet> getAll() {
        return pets.stream();
    }
}
